INSERT INTO
    currency (code, name)
VALUES
('840', 'USD'),
('980', 'UAH'),
('978', 'EUR'),
('404', 'KES')
;


INSERT INTO
    country (code, name, currency_id)
VALUES
('840', 'US', 1),
('804', 'UA', 2),
('040', 'AT', 3),
('056', 'BE', 3),
('380', 'IT', 3),
('404', 'KE', 4)
;