CREATE TABLE country
(
    id          BIGINT GENERATED BY DEFAULT AS IDENTITY NOT NULL,
    code        VARCHAR(255)                            NOT NULL,
    name        VARCHAR(255)                            NOT NULL,
    currency_id BIGINT,
    CONSTRAINT pk_country PRIMARY KEY (id)
);

CREATE TABLE currency
(
    id   BIGINT GENERATED BY DEFAULT AS IDENTITY NOT NULL,
    code VARCHAR(255)                            NOT NULL,
    name VARCHAR(255)                            NOT NULL,
    CONSTRAINT pk_currency PRIMARY KEY (id)
);

CREATE TABLE player
(
    id           BIGINT GENERATED BY DEFAULT AS IDENTITY NOT NULL,
    display_name VARCHAR(255)                            NOT NULL,
    email        VARCHAR(255)                            NOT NULL,
    password     VARCHAR(255)                            NOT NULL,
    country_id   BIGINT,
    CONSTRAINT pk_player PRIMARY KEY (id)
);

CREATE TABLE user_role
(
    user_id BIGINT NOT NULL,
    roles   VARCHAR
);

ALTER TABLE user_role
    ADD CONSTRAINT fk_user_role_on_player FOREIGN KEY (user_id) REFERENCES player (id);

ALTER TABLE country
    ADD CONSTRAINT FK_COUNTRY_ON_CURRENCY FOREIGN KEY (currency_id) REFERENCES currency (id);

ALTER TABLE player
    ADD CONSTRAINT FK_PLAYER_ON_COUNTRY FOREIGN KEY (country_id) REFERENCES country (id);