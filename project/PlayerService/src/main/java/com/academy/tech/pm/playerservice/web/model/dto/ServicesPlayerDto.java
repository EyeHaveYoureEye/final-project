package com.academy.tech.pm.playerservice.web.model.dto;

import lombok.Builder;
import lombok.Value;

@Builder
@Value
public class ServicesPlayerDto {

  String displayName;
  String countryName;
  String currencyName;

}
