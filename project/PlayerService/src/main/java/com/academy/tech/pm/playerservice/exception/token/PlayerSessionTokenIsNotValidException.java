package com.academy.tech.pm.playerservice.exception.token;

import com.academy.tech.pm.playerservice.exception.BadRequestException;

public class PlayerSessionTokenIsNotValidException extends BadRequestException {

  public PlayerSessionTokenIsNotValidException(String message) {
    super(message);
  }

}
