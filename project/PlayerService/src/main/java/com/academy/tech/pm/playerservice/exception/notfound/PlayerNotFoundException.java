package com.academy.tech.pm.playerservice.exception.notfound;

import com.academy.tech.pm.playerservice.exception.BadRequestException;

public class PlayerNotFoundException extends BadRequestException {

  public PlayerNotFoundException(String message) {
    super(message);
  }

}
