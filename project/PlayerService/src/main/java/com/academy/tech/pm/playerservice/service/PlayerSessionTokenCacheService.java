package com.academy.tech.pm.playerservice.service;

import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
public class PlayerSessionTokenCacheService implements CacheService<String, String> {

  @CachePut(cacheNames = "playerSessionTokenCache", key = "#playerDisplayName")
  public String savePlayerSessionTokenByDisplayName(String playerDisplayName, String sessionToken) {
    return sessionToken;
  }

  @CachePut(cacheNames = "sessionTokenPlayerCache", key = "#playerSessionToken")
  public String saveDisplayNameByPlayerSessionToken(String playerSessionToken, String playerDisplayName) {
    return playerDisplayName;
  }

  /**
   * Note: this function triggers 'playerSessionTokenCache'. You must trigger also
   * 'sessionTokenPlayerCache' to make expire time equals.
   * @param playerDisplayName
   * @return session token
   */
  @Override
  @Cacheable(cacheNames = "playerSessionTokenCache", key = "#playerDisplayName", unless = "#result == null")
  public String getValueByKey(String playerDisplayName) {
    return null;
  }

  /**
   * Note: this function triggers 'sessionTokenPlayerCache'. You must trigger also
   * 'playerSessionTokenCache' to make expire time equals.
   * @param playerSessionToken
   * @return player display name
   */
  @Override
  @Cacheable(cacheNames = "sessionTokenPlayerCache", key = "#playerSessionToken", unless = "#result == null")
  public String getKeyByValue(String playerSessionToken) {
    return null;
  }

}
