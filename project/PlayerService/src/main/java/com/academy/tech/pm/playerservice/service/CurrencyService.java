package com.academy.tech.pm.playerservice.service;

import com.academy.tech.pm.playerservice.domain.model.Currency;
import com.academy.tech.pm.playerservice.domain.repository.CurrencyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CurrencyService {

  private final CurrencyRepository currencyRepository;

  public CurrencyService(CurrencyRepository currencyRepository) {
    this.currencyRepository = currencyRepository;
  }

  public Currency getById(Long id) {
    return currencyRepository.getById(id);
  }

  public void save(Currency currency) {
    currencyRepository.save(currency);
  }
}
