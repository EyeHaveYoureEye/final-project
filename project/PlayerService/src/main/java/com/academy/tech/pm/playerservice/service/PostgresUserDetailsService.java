package com.academy.tech.pm.playerservice.service;

import com.academy.tech.pm.playerservice.domain.model.Player;
import com.academy.tech.pm.playerservice.domain.repository.PlayerRepository;
import com.academy.tech.pm.playerservice.exception.notfound.PlayerNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.Objects.isNull;

@Service
public class PostgresUserDetailsService implements UserDetailsService {

  private final PlayerRepository playerRepository;

  private final PasswordEncoder passwordEncoder;

  @Value("${spring.security.user.name}")
  private String serviceUserName;

  @Value("${spring.security.user.password}")
  private String servicePassword;

  @Value("${spring.security.user.roles}")
  private String serviceRole;

  public PostgresUserDetailsService(PlayerRepository playerRepository, PasswordEncoder passwordEncoder) {
    this.playerRepository = playerRepository;
    this.passwordEncoder = passwordEncoder;
  }

  @Override
  public UserDetails loadUserByUsername(String displayName) throws UsernameNotFoundException {
    if(displayName.equals(serviceUserName)) {
      return new User (serviceUserName, passwordEncoder.encode(servicePassword), List.of(new SimpleGrantedAuthority(serviceRole)));
    }

    Player player = playerRepository.findPlayerByDisplayName(displayName);

    if (isNull(player)) {
      throw new PlayerNotFoundException("Player with name [" + displayName + "] not found");
    }

    return new User(player.getUsername(), player.getPassword(), List.of(new SimpleGrantedAuthority("USER")));
  }

}
