package com.academy.tech.pm.playerservice.exception.notfound;

import com.academy.tech.pm.playerservice.exception.BadRequestException;

public class CountryNotFoundException extends BadRequestException {

  public CountryNotFoundException(String message) {
    super(message);
  }

}
