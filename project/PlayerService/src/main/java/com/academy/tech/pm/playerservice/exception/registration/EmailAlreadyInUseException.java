package com.academy.tech.pm.playerservice.exception.registration;

import com.academy.tech.pm.playerservice.exception.BadRequestException;

public class EmailAlreadyInUseException extends BadRequestException {

  public EmailAlreadyInUseException(String message) {
    super(message);
  }

}
