package com.academy.tech.pm.playerservice.domain.repository;

import com.academy.tech.pm.playerservice.domain.model.Currency;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "currency", path = "currency")
public interface CurrencyRepository extends CrudRepository<Currency, Long> {

  Currency getById(@Param("id") Long id);

}
