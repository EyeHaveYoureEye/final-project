package com.academy.tech.pm.playerservice.transformer;

import com.academy.tech.pm.playerservice.web.model.dto.PlayerSessionTokenDto;
import org.springframework.stereotype.Component;

@Component
public class PlayerSessionTokenTransformer implements ModelTransformer<PlayerSessionTokenDto, String> {

  @Override
  public String asEntity(PlayerSessionTokenDto dto) {
    return dto.getSessionToken();
  }

  @Override
  public PlayerSessionTokenDto asDto(String entity) {
    return PlayerSessionTokenDto.builder()
        .sessionToken(entity)
        .build();
  }

}
