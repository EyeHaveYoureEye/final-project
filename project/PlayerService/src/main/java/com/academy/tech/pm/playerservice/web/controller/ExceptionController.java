package com.academy.tech.pm.playerservice.web.controller;

import com.academy.tech.pm.playerservice.exception.BadRequestException;
import com.academy.tech.pm.playerservice.web.model.dto.ErrorInfoDto;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletRequest;

@Log4j2
@ControllerAdvice
public class ExceptionController {

  @ExceptionHandler
  @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
  @ResponseBody
  public ErrorInfoDto handleCommonException(HttpServletRequest req, Exception exception) {
    log.error(exception);
    return ErrorInfoDto.builder()
        .exceptionMessage(exception.getMessage())
        .url(req.getRequestURL().toString())
        .build();

  }

  @ExceptionHandler
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ResponseBody
  public ErrorInfoDto handleBadRequestException(HttpServletRequest req, BadRequestException exception) {
    log.info(exception.getMessage());
    return ErrorInfoDto.builder()
        .exceptionMessage(exception.getMessage())
        .url(req.getRequestURL().toString())
        .build();
  }

}
