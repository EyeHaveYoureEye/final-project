package com.academy.tech.pm.playerservice.config.web.security;


import com.academy.tech.pm.playerservice.service.PostgresUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

  private final PostgresUserDetailsService postgresUserDetailsService;

  private final PasswordEncoder passwordEncoder;

  public WebSecurityConfig(PostgresUserDetailsService postgresUserDetailsService, PasswordEncoder passwordEncoder) {
    this.postgresUserDetailsService = postgresUserDetailsService;
    this.passwordEncoder = passwordEncoder;
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http
        .csrf().disable()
        .cors().disable()
        .sessionManagement().disable()
        .httpBasic()
        .and()
            .authorizeRequests()
            .antMatchers("/registration").permitAll()
            .antMatchers("/actuator/**").permitAll()
            .antMatchers("/login").hasAuthority("USER")
            .antMatchers("/service/**").hasAuthority("SERVICE")
            .anyRequest().authenticated();
  }

  @Override
  public void configure(AuthenticationManagerBuilder builder)
      throws Exception {
    builder.userDetailsService(postgresUserDetailsService)
        .passwordEncoder(passwordEncoder);
  }

}
