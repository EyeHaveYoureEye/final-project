package com.academy.tech.pm.playerservice.exception.login;

import com.academy.tech.pm.playerservice.exception.UnauthorizedException;

public class LoginException extends UnauthorizedException {

  public LoginException(String message) {
    super(message);
  }

}
