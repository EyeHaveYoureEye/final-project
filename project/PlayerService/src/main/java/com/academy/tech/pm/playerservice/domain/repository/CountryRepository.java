package com.academy.tech.pm.playerservice.domain.repository;

import com.academy.tech.pm.playerservice.domain.model.Country;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "country", path = "country")
public interface CountryRepository extends CrudRepository<Country, Long> {

  Country getById(@Param("id") Long id);
  Country findCountryByCode(String code);

}
