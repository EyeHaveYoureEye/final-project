package com.academy.tech.pm.playerservice.service;

import com.academy.tech.pm.playerservice.domain.model.Country;
import com.academy.tech.pm.playerservice.domain.model.Player;
import com.academy.tech.pm.playerservice.domain.model.Role;
import com.academy.tech.pm.playerservice.domain.repository.CountryRepository;
import com.academy.tech.pm.playerservice.domain.repository.PlayerRepository;
import com.academy.tech.pm.playerservice.exception.notfound.CountryNotFoundException;
import com.academy.tech.pm.playerservice.exception.registration.EmailAlreadyInUseException;
import com.academy.tech.pm.playerservice.exception.registration.NameAlreadyInUseException;
import com.academy.tech.pm.playerservice.transformer.CountryTransformer;
import com.academy.tech.pm.playerservice.transformer.PlayerTransformer;
import com.academy.tech.pm.playerservice.web.model.dto.PlayerDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Set;

import static java.util.Objects.isNull;

@Service
public class PlayerService {

  private final PlayerRepository playerRepository;

  private final CountryRepository countryRepository;

  private final PlayerTransformer playerTransformer;

  private final CountryTransformer countryTransformer;

  private final PasswordEncoder passwordEncoder;

  public PlayerService(PlayerRepository playerRepository,
                       CountryRepository countryRepository,
                       PlayerTransformer playerTransformer,
                       CountryTransformer countryTransformer,
                       PasswordEncoder passwordEncoder) {
    this.playerRepository = playerRepository;
    this.countryRepository = countryRepository;
    this.playerTransformer = playerTransformer;
    this.countryTransformer = countryTransformer;
    this.passwordEncoder = passwordEncoder;
  }

  public void addPlayer(PlayerDto playerDto) {
    // check player displayName
    String playerDisplayName = playerDto.getDisplayName();
    Player playerByDisplayName = playerRepository.findPlayerByDisplayName(playerDisplayName);
    if (!isNull(playerByDisplayName)) {
      throw new NameAlreadyInUseException("Player with NAME [" + playerDisplayName + "] already exists.");
    }
    // check player email
    String playerEmail = playerDto.getEmail();
    Player playerByEmail = playerRepository.findPlayerByEmail(playerEmail);
    if (!isNull(playerByEmail)) {
      throw new EmailAlreadyInUseException("Player with EMAIL [" + playerEmail + "] already exists.");
    }
    // check country
    String countryCode = playerDto.getCountryDto().getCode();
    Country countryByCode = countryRepository.findCountryByCode(countryCode);
    if (isNull(countryByCode)) {
      throw new CountryNotFoundException("Country code [" + countryCode + "] do not exist");
    }
    // if everything is ok
    // set country from db in playerdto to get full country details
    playerDto.setCountryDto(countryTransformer.asDto(countryByCode));
    // encode password
    encodePassword(playerDto);
    Player playerToAdd = playerTransformer.asEntity(playerDto);
    // set USER role for player
    playerToAdd.setRoles(Set.of(Role.USER));
    playerRepository.save(playerToAdd);
  }

  public PlayerDto getPlayerByDisplayName(String displayName) {
    return playerTransformer.asDto(playerRepository.findPlayerByDisplayName(displayName));
  }

  private void encodePassword(PlayerDto playerDto) {
    playerDto.setPassword(passwordEncoder.encode(playerDto.getPassword()));
  }

}
