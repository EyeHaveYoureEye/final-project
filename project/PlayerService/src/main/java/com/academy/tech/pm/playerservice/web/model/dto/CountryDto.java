package com.academy.tech.pm.playerservice.web.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CountryDto {

  Long id;
  String code;
  String name;
  CurrencyDto currencyDto;

}
