package com.academy.tech.pm.playerservice.domain.repository;

import com.academy.tech.pm.playerservice.domain.model.Player;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PlayerRepository extends JpaRepository<Player, Long> {

  Player findPlayerByEmail(String email);
  Player findPlayerByDisplayName(String displayName);

}
