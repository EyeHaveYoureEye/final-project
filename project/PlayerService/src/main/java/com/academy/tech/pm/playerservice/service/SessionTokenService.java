package com.academy.tech.pm.playerservice.service;

public interface SessionTokenService<K, V> {

  V getTokenByKey(K key);
  K getKeyByToken(V token);
  String generateSessionToken();

}
