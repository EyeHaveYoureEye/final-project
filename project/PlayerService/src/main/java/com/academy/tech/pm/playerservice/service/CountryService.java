package com.academy.tech.pm.playerservice.service;

import com.academy.tech.pm.playerservice.domain.model.Country;
import com.academy.tech.pm.playerservice.domain.repository.CountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CountryService {

  private final CountryRepository countryRepository;

  public CountryService(CountryRepository countryRepository) {
    this.countryRepository = countryRepository;
  }

  public Country getById(Long id) {
    return countryRepository.getById(id);
  }

  public void save(Country country) {
    countryRepository.save(country);
  }

}
