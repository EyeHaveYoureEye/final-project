package com.academy.tech.pm.playerservice.domain.model;

import org.springframework.security.core.GrantedAuthority;

public enum Role implements GrantedAuthority {
  USER, SERVICE;

  @Override
  public String getAuthority() {
    return name();
  }
}
