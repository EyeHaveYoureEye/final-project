package com.academy.tech.pm.playerservice.service;

public interface CacheService<K, V> {

  K getKeyByValue(V value);
  V getValueByKey(K key);

}
