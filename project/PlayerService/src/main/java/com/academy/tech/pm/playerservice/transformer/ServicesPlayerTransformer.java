package com.academy.tech.pm.playerservice.transformer;

import com.academy.tech.pm.playerservice.web.model.dto.ServicesPlayerDto;
import com.academy.tech.pm.playerservice.web.model.dto.PlayerDto;
import org.springframework.stereotype.Component;

@Component
public class ServicesPlayerTransformer implements ModelTransformer<ServicesPlayerDto, PlayerDto> {

  @Override
  public PlayerDto asEntity(ServicesPlayerDto dto) {
    return null;
  }

  @Override
  public ServicesPlayerDto asDto(PlayerDto entity) {
    return ServicesPlayerDto.builder()
        .displayName(entity.getDisplayName())
        .countryName(entity.getCountryDto().getName())
        .currencyName(entity.getCountryDto().getCurrencyDto().getName())
        .build();
  }

}
