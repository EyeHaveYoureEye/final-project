package com.academy.tech.pm.playerservice.service;

import com.academy.tech.pm.playerservice.exception.token.PlayerSessionTokenIsNotValidException;
import com.academy.tech.pm.playerservice.transformer.ServicesPlayerTransformer;
import com.academy.tech.pm.playerservice.transformer.PlayerSessionTokenTransformer;
import com.academy.tech.pm.playerservice.web.model.dto.ServicesPlayerDto;
import com.academy.tech.pm.playerservice.web.model.dto.PlayerDto;
import com.academy.tech.pm.playerservice.web.model.dto.PlayerSessionTokenDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.UUID;

import static java.util.Objects.isNull;

@Service
public class PlayerSessionTokenService implements SessionTokenService<String, String> {

  private final PlayerService playerService;

  private final PlayerSessionTokenCacheService playerSessionTokenCacheService;

  private final PlayerSessionTokenTransformer playerSessionTokenTransformer;

  private final ServicesPlayerTransformer servicesPlayerTransformer;

  public PlayerSessionTokenService(PlayerService playerService,
                                   PlayerSessionTokenCacheService playerSessionTokenCacheService,
                                   PlayerSessionTokenTransformer playerSessionTokenTransformer,
                                   ServicesPlayerTransformer servicesPlayerTransformer) {
    this.playerService = playerService;
    this.playerSessionTokenCacheService = playerSessionTokenCacheService;
    this.playerSessionTokenTransformer = playerSessionTokenTransformer;
    this.servicesPlayerTransformer = servicesPlayerTransformer;
  }

  @Override
  public String generateSessionToken() {
    return UUID.randomUUID().toString();
  }

  @Override
  public String getTokenByKey(String playerDisplayName) {
    String token = playerSessionTokenCacheService.getValueByKey(playerDisplayName);
    if (isNull(token)) {
      token = playerSessionTokenCacheService
          .savePlayerSessionTokenByDisplayName(playerDisplayName, generateSessionToken());
      playerSessionTokenCacheService.saveDisplayNameByPlayerSessionToken(token, playerDisplayName);
    }
    playerSessionTokenCacheService.getKeyByValue(token);
    return token;
  }

  @Override
  public String getKeyByToken(String playerSessionToken) {
    String playerDisplayName = playerSessionTokenCacheService.getKeyByValue(playerSessionToken);
    if(isNull(playerDisplayName)) {
      throw new PlayerSessionTokenIsNotValidException("Player session token is not valid");
    }
    playerSessionTokenCacheService.getValueByKey(playerDisplayName);
    return playerDisplayName;
  }

  public PlayerSessionTokenDto getPlayerSessionTokenByDisplayName(String playerDisplayName) {
    return playerSessionTokenTransformer.asDto(getTokenByKey(playerDisplayName));
  }

  public PlayerDto getPlayerDtoBySessionToken(String sessionToken) {
    return playerService.getPlayerByDisplayName(getKeyByToken(sessionToken));
  }

  public ServicesPlayerDto getLobbyPlayerDtoBySessionToken(String sessionToken) {
    return servicesPlayerTransformer.asDto(playerService.getPlayerByDisplayName(getKeyByToken(sessionToken)));
  }

}
