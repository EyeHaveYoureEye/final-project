package com.academy.tech.pm.playerservice.web.model.dto;

import lombok.Builder;
import lombok.Value;

import java.io.Serializable;

@Builder
@Value
public class PlayerSessionTokenDto implements Serializable {

  String sessionToken;

}
