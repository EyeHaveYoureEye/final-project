package com.academy.tech.pm.playerservice.web.controller;

import com.academy.tech.pm.playerservice.service.PlayerService;
import com.academy.tech.pm.playerservice.web.model.dto.PlayerDto;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@Log4j2
@RestController
public class  RegistrationController {

  private final PlayerService playerService;

  public RegistrationController(PlayerService playerService) {
    this.playerService = playerService;
  }

  @PostMapping("/registration")
  @ResponseStatus(HttpStatus.CREATED)
  public void addPlayer(@RequestBody PlayerDto playerDto) {
    playerService.addPlayer(playerDto);
    log.info("[" + playerDto.getDisplayName() + "] registered.");
  }

}
