package com.academy.tech.pm.playerservice.transformer;

import com.academy.tech.pm.playerservice.domain.model.Country;
import com.academy.tech.pm.playerservice.domain.model.Player;
import com.academy.tech.pm.playerservice.web.model.dto.CountryDto;
import com.academy.tech.pm.playerservice.web.model.dto.PlayerDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PlayerTransformer implements ModelTransformer<PlayerDto, Player> {

  private final CountryTransformer countryTransformer;

  public PlayerTransformer(CountryTransformer countryTransformer) {
    this.countryTransformer = countryTransformer;
  }

  @Override
  public PlayerDto asDto(Player entity) {
    CountryDto countryDto = countryTransformer.asDto(entity.getCountry());
    return PlayerDto.builder()
        .id(entity.getId())
        .displayName(entity.getDisplayName())
        .email(entity.getEmail())
        .password(entity.getPassword())
        .countryDto(countryDto)
        .build();
  }

  @Override
  public Player asEntity(PlayerDto dto) {
    Country country = countryTransformer.asEntity(dto.getCountryDto());
    return new Player(dto.getId(), dto.getDisplayName(), dto.getEmail(), dto.getPassword(), country, null);
  }

}
