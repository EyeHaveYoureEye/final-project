package com.academy.tech.pm.playerservice.web.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;
import lombok.Value;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ErrorInfoDto {

  String exceptionMessage;
  String url;

}

