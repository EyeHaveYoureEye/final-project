package com.academy.tech.pm.playerservice.web.controller;

import com.academy.tech.pm.playerservice.service.PlayerSessionTokenService;
import com.academy.tech.pm.playerservice.web.model.dto.ServicesPlayerDto;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@Log4j2
@RestController
public class PlayerDetailsForServicesController {

  private final PlayerSessionTokenService playerSessionTokenService;

  public PlayerDetailsForServicesController(PlayerSessionTokenService playerSessionTokenService) {
    this.playerSessionTokenService = playerSessionTokenService;
  }

  @GetMapping("/service/player/{sessionToken}")
  public ServicesPlayerDto getPlayerByPlayerSessionTokenByLobby(
      @PathVariable(name = "sessionToken") String playerSessionToken) {
    log.info("Player with token [" + playerSessionToken + "] requested.");
    return playerSessionTokenService.getLobbyPlayerDtoBySessionToken(playerSessionToken);
  }

}
