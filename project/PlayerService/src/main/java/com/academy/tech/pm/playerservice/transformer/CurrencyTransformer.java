package com.academy.tech.pm.playerservice.transformer;

import com.academy.tech.pm.playerservice.domain.model.Currency;
import com.academy.tech.pm.playerservice.web.model.dto.CurrencyDto;
import org.springframework.stereotype.Component;

@Component
public class CurrencyTransformer implements ModelTransformer<CurrencyDto, Currency> {

  @Override
  public Currency asEntity(CurrencyDto currencyDto) {
    return new Currency(currencyDto.getId(), currencyDto.getCode(), currencyDto.getName());
  }

  @Override
  public CurrencyDto asDto(Currency currency) {
    return CurrencyDto.builder()
        .id(currency.getId())
        .code(currency.getCode())
        .name(currency.getName())
        .build();
  }

}
