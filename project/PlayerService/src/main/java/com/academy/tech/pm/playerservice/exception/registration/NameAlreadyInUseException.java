package com.academy.tech.pm.playerservice.exception.registration;

import com.academy.tech.pm.playerservice.exception.BadRequestException;

public class NameAlreadyInUseException extends BadRequestException {

  public NameAlreadyInUseException(String message) {
    super(message);
  }

}
