package com.academy.tech.pm.playerservice.transformer;

public interface ModelTransformer<D, E> {

  E asEntity(D dto);
  D asDto(E entity);

}
