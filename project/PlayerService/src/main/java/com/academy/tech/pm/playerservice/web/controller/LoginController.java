package com.academy.tech.pm.playerservice.web.controller;

import com.academy.tech.pm.playerservice.service.PlayerSessionTokenService;
import com.academy.tech.pm.playerservice.web.model.dto.PlayerSessionTokenDto;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@Log4j2
@RestController
public class LoginController {

  private final PlayerSessionTokenService playerSessionTokenService;

  public LoginController(PlayerSessionTokenService playerSessionTokenService) {
    this.playerSessionTokenService = playerSessionTokenService;
  }

  @GetMapping("/login")
  public PlayerSessionTokenDto login() {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    String playerDisplayName = auth.getName();
    PlayerSessionTokenDto playerSessionTokenByDisplayName =
        playerSessionTokenService.getPlayerSessionTokenByDisplayName(playerDisplayName);
    log.info("[" + playerDisplayName + "] logged and get [" + playerSessionTokenByDisplayName + "] token.");
    return playerSessionTokenByDisplayName;
  }

}
