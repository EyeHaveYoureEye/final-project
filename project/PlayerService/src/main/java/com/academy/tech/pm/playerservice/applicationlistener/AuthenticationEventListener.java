package com.academy.tech.pm.playerservice.applicationlistener;

import com.academy.tech.pm.playerservice.exception.login.LoginException;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AbstractAuthenticationEvent;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

@Log4j2
@Component
public class AuthenticationEventListener implements ApplicationListener<AbstractAuthenticationEvent> {

  @Override
  public void onApplicationEvent(AbstractAuthenticationEvent authenticationEvent) {
    Authentication authentication = authenticationEvent.getAuthentication();
    if (!authentication.isAuthenticated()) {
      throw new LoginException("Wrong password");
    }
    log.info("Login attempt with username [" + authentication.getName() +
        "]\tSuccess: " + authentication.isAuthenticated());
  }

}