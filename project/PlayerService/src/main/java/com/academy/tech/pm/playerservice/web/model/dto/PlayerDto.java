package com.academy.tech.pm.playerservice.web.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PlayerDto {

  Long id;
  String displayName;
  String email;
  String password;
  @JsonProperty("country")
  CountryDto countryDto;

}
