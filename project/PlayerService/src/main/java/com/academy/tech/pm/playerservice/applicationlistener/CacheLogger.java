package com.academy.tech.pm.playerservice.applicationlistener;

import lombok.extern.log4j.Log4j2;
import org.ehcache.event.CacheEvent;
import org.ehcache.event.CacheEventListener;
import org.springframework.stereotype.Component;

@Log4j2
@Component
public class CacheLogger implements CacheEventListener<String, String> {

  @Override
  public void onEvent(CacheEvent<? extends String, ? extends String> cacheEvent) {
    log.info(String.format("Cache event: key [%s] oldValue [%s] newValue [%s]",
        cacheEvent.getKey(), cacheEvent.getOldValue(), cacheEvent.getNewValue()));
  }

}
