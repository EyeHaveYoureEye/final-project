package com.academy.tech.pm.playerservice.transformer;

import com.academy.tech.pm.playerservice.domain.model.Country;
import com.academy.tech.pm.playerservice.domain.model.Currency;
import com.academy.tech.pm.playerservice.web.model.dto.CountryDto;
import com.academy.tech.pm.playerservice.web.model.dto.CurrencyDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component

public class CountryTransformer implements ModelTransformer<CountryDto, Country> {

  private final CurrencyTransformer currencyTransformer;

  public CountryTransformer(CurrencyTransformer currencyTransformer) {
    this.currencyTransformer = currencyTransformer;
  }

  @Override
  public CountryDto asDto(Country entity) {
    CurrencyDto currencyDto = CurrencyDto.builder()
        .id(entity.getCurrency().getId())
        .code(entity.getCurrency().getCode())
        .name(entity.getCurrency().getName())
        .build();

    return CountryDto.builder()
        .id(entity.getId())
        .code(entity.getCode())
        .name(entity.getName())
        .currencyDto(currencyDto)
        .build();
  }

  @Override
  public Country asEntity(CountryDto dto) {
    Currency currency = currencyTransformer.asEntity(dto.getCurrencyDto());
    return new Country(dto.getId(), dto.getCode(), dto.getName(), currency);
  }

}
