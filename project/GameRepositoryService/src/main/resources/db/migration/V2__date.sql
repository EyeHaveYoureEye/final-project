INSERT INTO
    provider (id, name, real_id, details, launch_url)
VALUES
(default, 'egt', 'egt', 'casino games', 'egt-interactive.com'),
(default, 'amatic', 'amatic', 'designer and distributor of gaming technology', 'www.amatic.com'),
(default, 'slotegrator-onetouch', 'slotegrator-onetouch', 'slot, table and live games supplier', 'www.onetouch.io')
;

INSERT INTO
    currency (id, name, code)
VALUES
(default, 'USD', '840'),
(default, 'UAH', '980'),
(default, 'EUR', '978'),
(default, 'KES', '404')
;

INSERT INTO
    country (id, name, code)
VALUES
(default, 'US', '840'),
(default, 'UA', '804'),
(default, 'AT', '040'),
(default, 'BE', '056'),
(default, 'IT', '380'),
(default, 'KE', '404')
;

INSERT INTO
    game (id, name, currency_id, provider_id)
VALUES
(default, 'Secrets-of-Sherwood', 1, 1),
(default, 'Lucky-Wood', 2, 1),
(default, 'Dark-Queen', 2, 1),
(default, 'Ice-Valley', 3, 1),
(default, 'The-Story-of-Alexander', 3, 1),
(default, 'Penguin-Style', 3, 1),
(default, 'Hot-&-Cash', 3, 1),
(default, 'Lady-Fruits-40-Easter', 1, 2),
(default, 'Plenty-Dragons', 1, 2),
(default, 'Book-of-Fruits', 2, 2),
(default, 'Lucky-Joker-20', 2, 2),
(default, 'Golden-Quest', 2, 2),
(default, 'Hot-81', 3, 2),
(default, 'Ice-Valley', 3, 2),
(default, 'Dark-Queen', 3, 2),
(default, 'Lady-Fruits 40 Easter', 1, 3),
(default, 'Wacky-Wildlife', 1, 3),
(default, 'Traveling-Treasures-India', 1, 3),
(default, 'Roulette', 2, 3),
(default, 'Pips-Quest', 2, 3),
(default, 'Hot-81', 3, 3),
(default, 'The-Maiden', 3, 3),
(default, 'GanBaruto-Battle', 3, 3),
(default, 'In-Between-Poker', 3, 3),
(default, 'Baccarat-No-Commission', 3, 3),
(default, 'Queens-of-Glory', 4, 3)
;

INSERT INTO
    provider_country (provider_id, country_id)
VALUES
(1, 1),
(1, 2),
(1, 4),
(1, 5),
(2, 1),
(2, 2),
(2, 3),
(2, 5),
(3, 1),
(3, 2),
(3, 3),
(3, 4),
(3, 5),
(3, 6)
;