package tech.edu.pm.GameRepo.web.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import tech.edu.pm.GameRepo.web.model.dto.ProviderLaunchUrlWIthGameNameDtoForResponse;

public interface LaunchUrlController {
  @GetMapping(path = "/launch")
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  ProviderLaunchUrlWIthGameNameDtoForResponse getLaunchUrl(@RequestParam(name = "lobbyGameId") String lobbyGameId,
                                                                  @RequestParam(name = "sessionKey") String sessionKey,
                                                                  @RequestParam(name = "hash") String hash);
}
