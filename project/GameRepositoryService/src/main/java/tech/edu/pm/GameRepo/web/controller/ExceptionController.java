package tech.edu.pm.GameRepo.web.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.client.RestClientException;
import tech.edu.pm.GameRepo.web.exceptions.EmptyObjectExceptions;
import tech.edu.pm.GameRepo.web.exceptions.GenerateHashExceptions;
import tech.edu.pm.GameRepo.web.exceptions.model.ErrorInfo;

import javax.servlet.http.HttpServletRequest;

public interface ExceptionController {

  @ExceptionHandler(value = Exception.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ResponseBody
  ErrorInfo handleException(HttpServletRequest req, Exception ex);

  @ExceptionHandler(value = RestClientException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ResponseBody
  ErrorInfo handleExceptionFromService(HttpServletRequest req, Exception ex);

  @ExceptionHandler(value = GenerateHashExceptions.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ResponseBody
  ErrorInfo handleGenerateHashExceptions(HttpServletRequest req, Exception ex);

  @ExceptionHandler(value = EmptyObjectExceptions.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ResponseBody
  ErrorInfo handleEmptyObjectExceptions(HttpServletRequest req, Exception ex);

  @ExceptionHandler(value = SecurityException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ResponseBody
  ErrorInfo handleSecurityException(HttpServletRequest req, Exception ex);

}
