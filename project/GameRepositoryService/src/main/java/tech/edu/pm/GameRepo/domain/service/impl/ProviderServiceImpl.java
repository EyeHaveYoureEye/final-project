package tech.edu.pm.GameRepo.domain.service.impl;

import org.springframework.stereotype.Service;
import tech.edu.pm.GameRepo.domain.repository.ProviderRepository;
import tech.edu.pm.GameRepo.domain.service.ProviderService;
import tech.edu.pm.GameRepo.domain.transformer.impl.ProviderLaunchUrlWIthGameNameDtoTransformerImpl;
import tech.edu.pm.GameRepo.web.exceptions.EmptyObjectExceptions;
import tech.edu.pm.GameRepo.web.model.dto.ProviderLaunchUrlWIthGameNameDto;

@Service
public class ProviderServiceImpl implements ProviderService {

  ProviderRepository providerRepository;

  ProviderLaunchUrlWIthGameNameDtoTransformerImpl providerLaunchUrlWIthGameNameDtoTransformer;

  public ProviderServiceImpl(ProviderRepository providerRepository, ProviderLaunchUrlWIthGameNameDtoTransformerImpl providerLaunchUrlWIthGameNameDtoTransformer) {
    this.providerRepository = providerRepository;
    this.providerLaunchUrlWIthGameNameDtoTransformer = providerLaunchUrlWIthGameNameDtoTransformer;
  }

  private static final String EMPTY_OBJECT_MESSAGE = "Object is empty: Class - %s, method - %s";

  @Override
  public ProviderLaunchUrlWIthGameNameDto findLaunchUrlAndGameNameByConcatenation_query(String concatenation) {
      ProviderLaunchUrlWIthGameNameDto providerLaunchUrlWIthGameNameDto = providerLaunchUrlWIthGameNameDtoTransformer.entityToDto(providerRepository.findLaunchUrlAndGameNameByConcatenation_query(concatenation));
      if (providerLaunchUrlWIthGameNameDto != null){
        return providerLaunchUrlWIthGameNameDto;
      }else {
        throw new EmptyObjectExceptions(String.format(EMPTY_OBJECT_MESSAGE,ProviderServiceImpl.class.getSimpleName(),ProviderServiceImpl.class.getDeclaredMethods()[0].getName()));
      }
  }



}
