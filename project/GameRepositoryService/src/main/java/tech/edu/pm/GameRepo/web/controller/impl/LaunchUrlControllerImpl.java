package tech.edu.pm.GameRepo.web.controller.impl;

import org.springframework.stereotype.Controller;
import tech.edu.pm.GameRepo.domain.service.LaunchUrlService;
import tech.edu.pm.GameRepo.domain.service.PlayerService;
import tech.edu.pm.GameRepo.domain.service.ProviderService;
import tech.edu.pm.GameRepo.web.controller.LaunchUrlController;
import tech.edu.pm.GameRepo.web.exceptions.EmptyObjectExceptions;
import tech.edu.pm.GameRepo.web.model.dto.PlayerDto;
import tech.edu.pm.GameRepo.web.model.dto.ProviderLaunchUrlWIthGameNameDto;
import tech.edu.pm.GameRepo.web.model.dto.ProviderLaunchUrlWIthGameNameDtoForResponse;

@Controller
public class LaunchUrlControllerImpl implements LaunchUrlController {


  private final ProviderService providerService;
  private final PlayerService playerService;
  private final LaunchUrlService launchUrlService;
  private static final String EMPTY_OBJECT_MESSAGE = "Object is empty: Class - %s, method - %s";

  public LaunchUrlControllerImpl(ProviderService providerService, PlayerService playerService, LaunchUrlService launchUrlService) {
    this.providerService = providerService;
    this.playerService = playerService;
    this.launchUrlService = launchUrlService;
  }

  public ProviderLaunchUrlWIthGameNameDtoForResponse getLaunchUrl(String lobbyGameId, String sessionKey, String hash) {

    if (launchUrlService.checkIsHashEqual(lobbyGameId, sessionKey, hash)) {
      ProviderLaunchUrlWIthGameNameDto providerLaunchUrlWIthGameNameDto = providerService.findLaunchUrlAndGameNameByConcatenation_query(lobbyGameId);
      PlayerDto playerDto = playerService.getPlayerInfoBySessionKey(sessionKey);
      if (providerLaunchUrlWIthGameNameDto != null && playerDto != null) {
        return launchUrlService.completeResponseAndGenerateHash(providerLaunchUrlWIthGameNameDto, playerDto, sessionKey);
      } else {
        throw new EmptyObjectExceptions(String.format(EMPTY_OBJECT_MESSAGE, LaunchUrlControllerImpl.class.getSimpleName(), LaunchUrlControllerImpl.class.getDeclaredMethods()[0].getName()));
      }
    } else {
      throw new SecurityException("Invalid hash from request");
    }
  }
}
