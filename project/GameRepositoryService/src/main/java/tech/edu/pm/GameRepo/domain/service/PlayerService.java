package tech.edu.pm.GameRepo.domain.service;

import tech.edu.pm.GameRepo.web.model.dto.PlayerDto;

public interface PlayerService {
  PlayerDto getPlayerInfoBySessionKey(String sessionKey);
}
