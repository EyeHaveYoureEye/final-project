package tech.edu.pm.GameRepo.domain.repository;

import tech.edu.pm.GameRepo.domain.model.repositoryEntity.Provider;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import tech.edu.pm.GameRepo.domain.model.servicesModels.ProviderLaunchUrlWIthGameName;

import java.util.List;

@Repository
public interface ProviderRepository extends JpaRepository<Provider, Integer> {

  List<Provider> findAllById(Integer id);

  @Query(nativeQuery = true)
  ProviderLaunchUrlWIthGameName findLaunchUrlAndGameNameByConcatenation_query(String concatenation);
}
