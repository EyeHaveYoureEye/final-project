package tech.edu.pm.GameRepo.domain.model.repositoryEntity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import tech.edu.pm.GameRepo.domain.model.servicesModels.ProviderLaunchUrlWIthGameName;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedNativeQuery;
import javax.persistence.OneToMany;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import java.util.Set;

@NamedNativeQuery(name = "Provider.findLaunchUrlAndGameNameByConcatenation_query"
,query =  "select concat.launch_url as first,concat.name as last  \n" +
        "  from (select p.launch_url,p.real_id,p.details, g.name,g.id,p.name || '-' || g.name p_g_name \n" +
        "    from game g left join provider p on g.provider_id = p.id) as concat \n" +
        "where concat.p_g_name = :concatenation"
,resultSetMapping = "Mapping.ProviderLaunchUrlWIthGameName")
@SqlResultSetMapping(name="Mapping.ProviderLaunchUrlWIthGameName",
classes = @ConstructorResult(targetClass = ProviderLaunchUrlWIthGameName.class,
columns = {
        @ColumnResult(name = "first"),
        @ColumnResult(name = "last")
}))

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "provider")
public class Provider {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;
  @Column(nullable = false)
  private String name;
  @Column(nullable = false)
  private String real_id;
  @Column
  private String details;
  @Column(nullable = false)
  private String launch_url;

  @EqualsAndHashCode.Exclude
  @ToString.Exclude
  @ManyToMany
  @JoinTable(
          name = "provider_country",
          joinColumns = @JoinColumn(name = "provider_id",referencedColumnName = "id"),
          inverseJoinColumns = @JoinColumn(name = "country_id",referencedColumnName = "id")
  )
  private Set<Country> providerCountry;

  @EqualsAndHashCode.Exclude
  @ToString.Exclude
  @OneToMany(mappedBy = "provider")
  private Set<Game> games;
}
