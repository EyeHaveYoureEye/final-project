package tech.edu.pm.GameRepo.web.controller.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.client.RestClientException;
import tech.edu.pm.GameRepo.web.controller.ExceptionController;
import tech.edu.pm.GameRepo.web.exceptions.EmptyObjectExceptions;
import tech.edu.pm.GameRepo.web.exceptions.GenerateHashExceptions;
import tech.edu.pm.GameRepo.web.exceptions.model.ErrorInfo;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
public class ExceptionControllerImpl implements ExceptionController {

  static final Logger log = LoggerFactory.getLogger(ExceptionControllerImpl.class);


  public ErrorInfo handleException(HttpServletRequest req, Exception ex) {
    log.error("Error from internal service by url: " + req.getRequestURL().toString() +
            "; exception message: " + ex.getMessage());
    return new ErrorInfo(req.getRequestURL().toString(), ex);
  }

  public ErrorInfo handleExceptionFromService(HttpServletRequest req, Exception ex) {
    log.error("Error from internal service by url: " + req.getRequestURL().toString() +
            "; exception message: " + ex.getMessage());
    return new ErrorInfo(req.getRequestURL().toString(), ex);
  }

  public ErrorInfo handleGenerateHashExceptions(HttpServletRequest req, Exception ex) {
    log.error("Error from internal service by url: " + req.getRequestURL().toString() +
            "; exception message: " + ex.getMessage());
    return new ErrorInfo(req.getRequestURL().toString(), ex);
  }

  public ErrorInfo handleEmptyObjectExceptions(HttpServletRequest req, Exception ex) {
    log.error("Error from internal service by url: " + req.getRequestURL().toString() +
            "; exception message: " + ex.getMessage());
    return new ErrorInfo(req.getRequestURL().toString(), ex);
  }

  public ErrorInfo handleSecurityException(HttpServletRequest req, Exception ex) {
    log.error("Error from internal service by url: " + req.getRequestURL().toString() +
            "; exception message: " + ex.getMessage());
    return new ErrorInfo(req.getRequestURL().toString(), ex);
  }

}
