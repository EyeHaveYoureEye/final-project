package tech.edu.pm.GameRepo.domain.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import tech.edu.pm.GameRepo.domain.service.LaunchUrlService;
import tech.edu.pm.GameRepo.web.exceptions.GenerateHashExceptions;
import tech.edu.pm.GameRepo.web.model.dto.PlayerDto;
import tech.edu.pm.GameRepo.web.model.dto.ProviderLaunchUrlWIthGameNameDto;
import tech.edu.pm.GameRepo.web.model.dto.ProviderLaunchUrlWIthGameNameDtoForResponse;

import javax.xml.bind.DatatypeConverter;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;

@Service
public class LaunchUrlServiceImpl implements LaunchUrlService {
  static final Logger log = LoggerFactory.getLogger(LaunchUrlServiceImpl.class);

  @Value("${secret-salt}")
  private String secretSalt;

  @Override
  public String concatenateUrl(ProviderLaunchUrlWIthGameNameDto providerLaunchUrlWIthGameNameDto, PlayerDto playerDto, String sessionKey) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("https://");
    stringBuilder.append(providerLaunchUrlWIthGameNameDto.getLaunchProviderUrl());
    stringBuilder.append("/launch?game=");
    stringBuilder.append(providerLaunchUrlWIthGameNameDto.getGameName());
    stringBuilder.append("&sessionToken=");
    stringBuilder.append(sessionKey);
    stringBuilder.append("&displayName=");
    stringBuilder.append(playerDto.getDisplayName());
    stringBuilder.append("&currency=");
    stringBuilder.append(playerDto.getCurrencyName());
    return stringBuilder.toString();
  }

  @Override
  public ProviderLaunchUrlWIthGameNameDtoForResponse completeResponseAndGenerateHash(ProviderLaunchUrlWIthGameNameDto providerLaunchUrlWIthGameNameDto, PlayerDto playerDto, String sessionKey) {
    ProviderLaunchUrlWIthGameNameDtoForResponse providerLaunchUrlWIthGameNameDtoForResponse = new ProviderLaunchUrlWIthGameNameDtoForResponse(concatenateUrl(providerLaunchUrlWIthGameNameDto,playerDto,sessionKey),
            providerLaunchUrlWIthGameNameDto.getLaunchProviderUrl(), providerLaunchUrlWIthGameNameDto.getGameName(),sessionKey,
            playerDto.getDisplayName(),playerDto.getCurrencyName(),
            generateHashFromString(generateStringForResponse(providerLaunchUrlWIthGameNameDto,playerDto,sessionKey)));
    log.info("Successfully create dto for response: Player name - " + playerDto.getDisplayName());
    return providerLaunchUrlWIthGameNameDtoForResponse;
  }

  @Override
  public String generateHashFromString(String line) {
    try {
      MessageDigest messageDigest = MessageDigest.getInstance("MD5");
      byte[] result = messageDigest.digest(line.getBytes(StandardCharsets.UTF_8));
      return DatatypeConverter.printHexBinary(result).toLowerCase(Locale.ROOT);
    }catch (NoSuchAlgorithmException noSuchAlgorithmException){
      throw new GenerateHashExceptions("Exceptions in generate hash");
    }
  }

  @Override
  public String generateStringForResponse(ProviderLaunchUrlWIthGameNameDto providerLaunchUrlWIthGameNameDto, PlayerDto playerDto, String sessionKey) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(playerDto.getCurrencyName());
    stringBuilder.append(playerDto.getDisplayName());
    stringBuilder.append(providerLaunchUrlWIthGameNameDto.getGameName());
    stringBuilder.append(providerLaunchUrlWIthGameNameDto.getLaunchProviderUrl());
    stringBuilder.append(sessionKey);
    stringBuilder.append(secretSalt);
    return stringBuilder.toString();
  }

  @Override
  public String generateStringForRequest(String lobbyGameId, String sessionKey) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(lobbyGameId);
    stringBuilder.append(sessionKey);
    stringBuilder.append(secretSalt);
    return stringBuilder.toString();
  }

  @Override
  public boolean checkIsHashEqual(String lobbyGameId, String sessionKey,String hash){
    boolean check = hash.equals(generateHashFromString(generateStringForRequest(lobbyGameId, sessionKey)));
    if (check) {
      log.info("The request passed the hash check");
    }
    return check;
  }

}
