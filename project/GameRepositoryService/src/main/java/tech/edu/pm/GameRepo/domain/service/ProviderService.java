package tech.edu.pm.GameRepo.domain.service;

import tech.edu.pm.GameRepo.web.model.dto.ProviderLaunchUrlWIthGameNameDto;

public interface ProviderService {
  ProviderLaunchUrlWIthGameNameDto findLaunchUrlAndGameNameByConcatenation_query(String concatenation);
}
