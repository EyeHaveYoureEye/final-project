package tech.edu.pm.GameRepo.web.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProviderLaunchUrlWIthGameNameDto {

  String launchProviderUrl;
  String gameName;

}
