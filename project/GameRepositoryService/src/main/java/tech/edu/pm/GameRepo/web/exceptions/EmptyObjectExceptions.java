package tech.edu.pm.GameRepo.web.exceptions;

public class EmptyObjectExceptions extends RuntimeException {
  public EmptyObjectExceptions(String message){ super(message);}
}
