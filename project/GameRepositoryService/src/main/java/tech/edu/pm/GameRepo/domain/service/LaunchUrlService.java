package tech.edu.pm.GameRepo.domain.service;

import tech.edu.pm.GameRepo.web.exceptions.GenerateHashExceptions;
import tech.edu.pm.GameRepo.web.model.dto.PlayerDto;
import tech.edu.pm.GameRepo.web.model.dto.ProviderLaunchUrlWIthGameNameDto;
import tech.edu.pm.GameRepo.web.model.dto.ProviderLaunchUrlWIthGameNameDtoForResponse;

public interface LaunchUrlService {
  String concatenateUrl(ProviderLaunchUrlWIthGameNameDto providerLaunchUrlWIthGameNameDto, PlayerDto playerDto, String sessionKey);
  ProviderLaunchUrlWIthGameNameDtoForResponse completeResponseAndGenerateHash(ProviderLaunchUrlWIthGameNameDto providerLaunchUrlWIthGameNameDto, PlayerDto playerDto, String sessionKey) throws GenerateHashExceptions;
  String generateHashFromString(String line);
  String generateStringForResponse(ProviderLaunchUrlWIthGameNameDto providerLaunchUrlWIthGameNameDto, PlayerDto playerDto, String sessionKey);
  String generateStringForRequest(String lobbyGameId, String sessionKey);
  boolean checkIsHashEqual(String lobbyGameId, String sessionKey,String hash);
}
