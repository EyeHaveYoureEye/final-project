package tech.edu.pm.GameRepo.domain.transformer;

public interface ModelTransformer<E,D> {
  D entityToDto(E entity);
  E dtoToEntity(D dto);
}
