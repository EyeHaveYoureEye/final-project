package tech.edu.pm.GameRepo.domain.model.servicesModels;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProviderLaunchUrlWIthGameName {

  String launchProviderUrl;
  String gameName;

}
