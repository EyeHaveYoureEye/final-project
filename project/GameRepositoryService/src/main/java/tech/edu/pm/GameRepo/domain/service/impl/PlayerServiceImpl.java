package tech.edu.pm.GameRepo.domain.service.impl;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.support.BasicAuthenticationInterceptor;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import tech.edu.pm.GameRepo.domain.service.PlayerService;
import tech.edu.pm.GameRepo.web.model.dto.PlayerDto;

import java.time.Duration;

@Service
public class PlayerServiceImpl implements PlayerService {


  private final RestTemplate restTemplate;

  @Value("${player-service-login}")
  private String login;
  @Value("${player-service-password}")
  private String password;

  @Value("${player-service.url}")
  private String baseUrlPlayerService;

  public PlayerServiceImpl(RestTemplateBuilder restTemplateBuilder) {
    this.restTemplate = restTemplateBuilder
    .setConnectTimeout(Duration.ofSeconds(500))
    .setReadTimeout(Duration.ofSeconds(500))
    .build();
  }

  public PlayerDto getPlayerInfoBySessionKey(String sessionKey){
    restTemplate.getInterceptors().add(new BasicAuthenticationInterceptor(login,password));
    ResponseEntity<PlayerDto> playerDtoResponseEntity = restTemplate.getForEntity(baseUrlPlayerService,PlayerDto.class,sessionKey);
    return playerDtoResponseEntity.getBody();
  }
}
