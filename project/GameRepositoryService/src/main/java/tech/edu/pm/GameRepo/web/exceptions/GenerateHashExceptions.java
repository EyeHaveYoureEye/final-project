package tech.edu.pm.GameRepo.web.exceptions;

public class GenerateHashExceptions extends RuntimeException {
  public GenerateHashExceptions(String message){ super(message);}
}
