package tech.edu.pm.GameRepo.domain.transformer.impl;

import org.springframework.stereotype.Component;
import tech.edu.pm.GameRepo.domain.model.servicesModels.ProviderLaunchUrlWIthGameName;
import tech.edu.pm.GameRepo.domain.transformer.ModelTransformer;
import tech.edu.pm.GameRepo.web.model.dto.ProviderLaunchUrlWIthGameNameDto;

@Component
public class ProviderLaunchUrlWIthGameNameDtoTransformerImpl implements ModelTransformer<ProviderLaunchUrlWIthGameName, ProviderLaunchUrlWIthGameNameDto> {
  @Override
  public ProviderLaunchUrlWIthGameNameDto entityToDto(ProviderLaunchUrlWIthGameName entity) {
    return ProviderLaunchUrlWIthGameNameDto.builder()
            .launchProviderUrl(entity.getLaunchProviderUrl())
            .gameName(entity.getGameName())
            .build();
  }

  @Override
  public ProviderLaunchUrlWIthGameName dtoToEntity(ProviderLaunchUrlWIthGameNameDto dto) {
    return new ProviderLaunchUrlWIthGameName(dto.getLaunchProviderUrl(), dto.getGameName());
  }
}
