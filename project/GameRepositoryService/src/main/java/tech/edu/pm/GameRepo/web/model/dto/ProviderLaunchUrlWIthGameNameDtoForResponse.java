package tech.edu.pm.GameRepo.web.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ProviderLaunchUrlWIthGameNameDtoForResponse {
  String url;
  String launchProviderUrl;
  String gameName;
  String sessionKey;
  String displayName;
  String currencyName;
  String hash;
}
